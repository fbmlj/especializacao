class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :registration
      t.time :class_time
      t.string :day
      t.references :teacher, foreign_key: true
      t.references :subject, foreign_key: true
      t.references :period, foreign_key: true

      t.timestamps
    end
  end
end
