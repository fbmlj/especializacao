class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.string :name
      t.string :registration
      t.integer :age
      t.references :department, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
