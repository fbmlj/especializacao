class CreateLessions < ActiveRecord::Migration[5.2]
  def change
    create_table :lessions do |t|
      t.date :lession_date
      t.time :lession_time
      t.references :classroom, foreign_key: true

      t.timestamps
    end
  end
end
