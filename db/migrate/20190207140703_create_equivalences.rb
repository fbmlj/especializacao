class CreateEquivalences < ActiveRecord::Migration[5.2]
  def change
    create_table :equivalences do |t|
      t.integer :subject_id
      t.integer :discipline_id

      t.timestamps
    end
  end
end
