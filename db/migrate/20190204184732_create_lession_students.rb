class CreateLessionStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :lession_students do |t|
      t.references :lession, foreign_key: true
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
