Rails.application.routes.draw do
  devise_for :users
  resources :users
  root to: 'pages#home'
  get 'pages/home'
  resources :equivalences
  resources :classroom_students
  resources :subject_courses
  resources :lession_students
  resources :periods
  resources :classrooms
  resources :lessions
  resources :students
  resources :teachers
  resources :subjects
  resources :courses
  resources :departments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
