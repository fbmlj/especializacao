class Student < ApplicationRecord
  belongs_to :course
  has_many :classroom_students
  has_many :lession_students
  has_many :classrooms, :through => :classroom_students
  has_many :lessions, :through => :lession_students
end
