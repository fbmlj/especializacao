class Equivalence < ApplicationRecord
  belongs_to :discipline, class_name: "Subject"
  belongs_to :subject
end
