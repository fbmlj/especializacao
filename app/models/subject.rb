class Subject < ApplicationRecord
  belongs_to :department
  has_many :classrooms
  has_many :subject_courses
  has_many :equivalences
  has_many :courses, through: :subject_courses
  has_many :disciplines, :through => :equivalences,:source => :discipline
end
