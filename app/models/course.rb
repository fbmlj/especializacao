class Course < ApplicationRecord
  belongs_to :department
  has_many :students
  has_many :subject_courses
  has_many :subject, :through => :subject_courses
end
