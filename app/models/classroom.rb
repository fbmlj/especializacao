class Classroom < ApplicationRecord
  belongs_to :teacher
  belongs_to :subject
  belongs_to :period
  has_many :classroom_students
  has_many :students, :through => :classroom_students
end
