class Lession < ApplicationRecord
  belongs_to :classroom
  has_many :lession_students
  has_many :students, :through =>:lession_student
end
