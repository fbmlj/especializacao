class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
    has_one :teacher
    mount_uploader :avatar, AvatarUploader
    enum kind: {
      standard: 2,
      admin: 1,
      coordenator: 3
    }
end
