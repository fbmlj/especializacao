class ClassroomStudentsController < ApplicationController
    before_action :set_classroom_student, only: [:destroy]
    def new
        
        @classroom_student=ClassroomStudent.new
        @classrooms=Classroom.all
        @students=Student.all
    end

    def create
        @classroom_student=ClassroomStudent.new(classroom_student_params)
        respond_to do |format|
            if @classroom_student.save
                format.html { redirect_to classrooms_path notice: 'Classroom and student was successfully created.' }
                format.json { render :show, status: :created, location: @classroom_student }
            else
                format.html { render :new }
                format.json { render json: @classroom_student.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @classroom_student.destroy
        respond_to do |format|
          format.html { redirect_to users_url, notice: 'classroom_student was successfully destroyed.' }
          format.json { head :no_content }
        end
    end
    private
        def classroom_student_params
            params.require(:classroom_student).permit(:classroom_id, :student_id)
        end
        
        def set_classroom_student
            @classroom_student=ClassroomStudent.find(params[:id])
        end

end


