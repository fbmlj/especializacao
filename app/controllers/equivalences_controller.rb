class EquivalencesController < ApplicationController
    before_action :set_equivalence, only: [:destroy]
    def new
        @equivalence=Equivalence.new
        @subjects=Subject.all
    end

    def create
        @equivalence=Equivalence.new(equivalence_params)
        respond_to do |format|
            if @equivalence.save
                format.html { redirect_to subjects_path, notice: 'Equivalence was successfully created.' }
                format.json { render :show, status: :created, location: @equivalence }
            else
                format.html { render :new }
                format.json { render json: @equivalence.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @equivalence.destroy
        respond_to do |format|
          format.html { redirect_to users_url, notice: 'equivalence was successfully destroyed.' }
          format.json { head :no_content }
        end
    end
    private
        def set_equivalence
            @equivalence=Equivalence.find(params[:id])
        end
    
        def equivalence_params
            params.require(:equivalence).permit(:subject_id, :discipline_id)
        end
        

end
