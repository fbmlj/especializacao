class LessionStudentsController < ApplicationController
    before_action :set_lession_student, only: [:destroy]
    def new
    
        @lession_student=LessionStudent.new
        @lessions=Lession.all
        @students=Student.all
    end

    def create
        @lession_student=LessionStudent.new(lession_student_params)
        respond_to do |format|
            if @lession_student.save
                format.html { redirect_to lessions_path notice: 'Lessions and student was successfully created.' }
                format.json { render :show, status: :created, location: @lession_student }
            else
                format.html { render :new }
                format.json { render json: @lession_student.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @lession_student.destroy
        respond_to do |format|
          format.html { redirect_to students_url, notice: 'lession_student was successfully destroyed.' }
          format.json { head :no_content }
        end
    end
    private
        def lession_student_params
            params.require(:lession_student).permit(:lession_id, :student_id)
        end
        def set_lession_student
            @lession_student = LessionStudent.find(params[:id])
        end


end

