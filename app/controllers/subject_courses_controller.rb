class SubjectCoursesController < ApplicationController
  before_action :set_subject_course, only: [:destroy]
  def new
      
      @subject_course=SubjectCourse.new
      @courses=Course.all
      @subjects=Subject.all
  end
  def create
      @subject_course=SubjectCourse.new(subject_course_params)
      respond_to do |format|
        if @subject_course.save
          format.html { redirect_to courses_path notice: 'Coursess and subject was successfully created.' }
          format.json { render :show, status: :created, location: @subject_course }
        else
          format.html { render :new }
          format.json { render json: @subject_course.errors, status: :unprocessable_entity }
        end
     end
  end

  def destroy
     @subject_course.destroy
     respond_to do |format|
       format.html { redirect_to users_url, notice: 'subject_course was successfully destroyed.' }
       format.json { head :no_content }
     end
  end
  private
    def subject_course_params
      params.require(:subject_course).permit(:course_id, :subject_id)
    end

    def set_subject_course
      @subject_course = SubjectCourse.find(params[:id])
    end

end
