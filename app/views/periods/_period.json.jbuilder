json.extract! period, :id, :name, :start_date, :created_at, :updated_at
json.url period_url(period, format: :json)
