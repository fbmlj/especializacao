json.extract! lession, :id, :lession_date, :lession_time, :classroom_id, :created_at, :updated_at
json.url lession_url(lession, format: :json)
