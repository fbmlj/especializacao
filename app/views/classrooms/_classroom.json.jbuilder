json.extract! classroom, :id, :registration, :class_time, :day, :teacher_id, :subject_id, :period_id, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
