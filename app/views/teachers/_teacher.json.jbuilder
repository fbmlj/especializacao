json.extract! teacher, :id, :name, :registration, :age, :department_id, :user_id, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)
