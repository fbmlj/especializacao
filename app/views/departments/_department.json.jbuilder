json.extract! department, :id, :name, :code, :address, :created_at, :updated_at
json.url department_url(department, format: :json)
